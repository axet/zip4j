package net.lingala.zip4j;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.attribute.PosixFileAttributeView;
import java.nio.file.attribute.PosixFilePermission;
import java.util.ArrayList;
import java.util.Set;

import net.lingala.zip4j.model.enums.RandomAccessFileMode;
import net.lingala.zip4j.util.FileUtils;
import net.lingala.zip4j.util.InternalZipConstants;

public class NativeStorage { // original java 'File', to easy port to android SAF
    protected File f;

    public interface FilenameFilter {
        boolean accept(NativeStorage dir, String name);
    }

    public NativeStorage(NativeStorage v) { // create copy by value
        f = new File(f.getPath());
    }

    public NativeStorage(File f) {
        this.f = f;
    }

    public NativeStorage(String f) {
        this.f = new File(f);
    }

    public NativeFile read() throws FileNotFoundException { // open file for reading
        return new NativeFile(f, RandomAccessFileMode.READ.getValue());
    }

    public NativeFile write() throws FileNotFoundException { // open file for writing
        return new NativeFile(f, RandomAccessFileMode.WRITE.getValue());
    }

    public NativeStorage open(String name) { // open current directory item
        return new NativeStorage(new File(f, name));
    }

    public boolean exists() {
        return f.exists();
    }

    public boolean canRead() {
        return f.canRead();
    }

    public boolean canWrite() {
        return f.canWrite();
    }

    public boolean isHidden() {
        return f.isHidden();
    }

    public NativeStorage getParent() {
        return new NativeStorage(f.getParentFile());
    }

    public NativeStorage getParentFile() { // keep names
        return getParent();
    }

    public String getName() {
        return f.getName();
    }

    public String getAbsolutePath() {
        return f.getAbsolutePath();
    }

    public boolean isDirectory() {
        return f.isDirectory();
    }

    public long lastModified() {
        return f.lastModified();
    }

    public long length() {
        return f.length();
    }

    public boolean renameTo(NativeStorage to) {
        return f.renameTo(to.f);
    }

    public void setLastModified(long l) {
        f.setLastModified(l);
    }

    public void setReadOnly() {
        f.setReadOnly();
    }

    public boolean mkdirs() {
        return f.mkdirs();
    }

    public boolean delete() {
        return f.delete();
    }

    public NativeStorage[] listFiles() {
        File[] ff = f.listFiles();
        if (ff == null)
            return null;
        NativeStorage[] nn = new NativeStorage[ff.length];
        for (int i = 0; i < nn.length; i++) {
            nn[i] = new NativeStorage(ff[i]);
        }
        return nn;
    }

    public NativeStorage[] listFiles(FilenameFilter filter) {
        File[] ff = f.listFiles();
        if (ff == null)
            return null;
        ArrayList<NativeStorage> nn = new ArrayList<>();
        for (int i = 0; i < ff.length; i++) {
            NativeStorage s = new NativeStorage(ff[i]);
            if (filter.accept(s.getParent(), s.getName()))
                nn.add(s);
        }
        return nn.toArray(new NativeStorage[0]);
    }

    public String getPath() {
        return f.getPath();
    }

    public String getCanonicalPath() throws IOException {
        return f.getCanonicalPath();
    }

    public NativeStorage getCanonicalFile() {
        return this;
    }

    public Path toPath() {
        return f.toPath();
    }

    public boolean renameTo(File to) {
        return f.renameTo(to);
    }

    public String getRelPath(NativeStorage child) {
        String rootFolderFileRef = getPath();

        if (!rootFolderFileRef.endsWith(InternalZipConstants.FILE_SEPARATOR)) {
            rootFolderFileRef += InternalZipConstants.FILE_SEPARATOR;
        }

        String filePath = child.getPath();
        String tmpFileName = filePath.substring(rootFolderFileRef.length());
        if (tmpFileName.startsWith(System.getProperty("file.separator"))) {
            tmpFileName = tmpFileName.substring(1);
        }

        return tmpFileName;
    }

    public boolean isSymbolicLink() {
        return Files.isSymbolicLink(f.toPath());
    }

    public Path readSymbolicLink() throws IOException {
        return Files.readSymbolicLink(f.toPath());
    }
    
    public byte[] getFileAttributes() {
        return FileUtils.getFileAttributes(toPath());
    }

}
