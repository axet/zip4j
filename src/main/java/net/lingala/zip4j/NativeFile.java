package net.lingala.zip4j;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

import net.lingala.zip4j.model.enums.RandomAccessFileMode;

public class NativeFile implements AutoCloseable { // native java 'RandomAccessFile', for easy port for android SAF
    protected RandomAccessFile r;

    public NativeFile() {
    }

    public NativeFile(File f, String mode) throws FileNotFoundException {
        this.r = new RandomAccessFile(f, mode);
    }

    public NativeFile(NativeStorage file, String mode) throws FileNotFoundException {
        this.r = new RandomAccessFile(file.f, mode);
    }

    public long length() throws IOException {
        return r.length();
    }

    public void seek(long s) throws IOException {
        r.seek(s);
    }
    
    public int skipBytes(int i) throws IOException {
        return r.skipBytes(i);
    }

    public void readFully(byte[] buf) throws IOException {
        r.readFully(buf);
    }

    public void readFully(byte[] buf, int off, int len) throws IOException {
        r.readFully(buf, off, len);
    }

    public int read() throws IOException {
        return r.read();
    }

    public int read(byte[] buf) throws IOException {
        return r.read(buf);
    }

    public int read(byte[] buf, int off, int len) throws IOException {
        return r.read(buf, off, len);
    }

    public void write(int b) throws IOException {
        r.write(b);
    }

    public long getFilePointer() throws IOException {
        return r.getFilePointer();
    }

    public void close() throws IOException {
        r.close();
    }

    public void write(byte[] buf) throws IOException {
        r.write(buf);
    }

    public void write(byte[] b, int off, int len) throws IOException {
        r.write(b, off, len);
    }
}
