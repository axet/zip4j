package net.lingala.zip4j.model;

import java.io.File;

import net.lingala.zip4j.NativeStorage;

public interface ExcludeFileFilter {

    boolean isExcluded(NativeStorage file);

}
