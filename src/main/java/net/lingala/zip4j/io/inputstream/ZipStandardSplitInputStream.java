package net.lingala.zip4j.io.inputstream;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import net.lingala.zip4j.NativeStorage;

/**
 * A split input stream for zip file split as per zip specification. They end with .z01, .z02... .zip
 */
public class ZipStandardSplitInputStream extends SplitInputStream {

  private int lastSplitZipFileNumber;

  public ZipStandardSplitInputStream(NativeStorage zipFile, boolean isSplitZipArchive, int lastSplitZipFileNumber) throws FileNotFoundException {
    super(zipFile, isSplitZipArchive, lastSplitZipFileNumber);
    this.lastSplitZipFileNumber = lastSplitZipFileNumber;
  }

  @Override
  protected NativeStorage getNextSplitFile(int zipFileIndex) throws IOException {
    if (zipFileIndex == lastSplitZipFileNumber) {
      return zipFile;
    }

    String currZipFileNameWithPath = zipFile.getName();
    String extensionSubString = ".z0";
    if (zipFileIndex >= 9) {
      extensionSubString = ".z";
    }

    return zipFile.open(currZipFileNameWithPath.substring(0,
        currZipFileNameWithPath.lastIndexOf(".")) + extensionSubString + (zipFileIndex + 1));
  }
}
